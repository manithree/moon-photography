#!/usr/bin/python3

import ephem
import sys,os
import datetime
import configparser
import matplotlib
matplotlib.use('WXAgg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

config = configparser.ConfigParser()
config.read("{HOME}/.config/mooncal.ini".format(HOME=os.environ['HOME']))

home = ephem.Observer()
home.lat = config['location']['lat']
home.lon = config['location']['lon']
home_tz = config['location']['timezone']

start_date = ephem.Date(sys.argv[1])
num_days =int(sys.argv[2])

interval = ephem.hour*24

batch =[]
for day in range(0,num_days):
    home.date = start_date
    moon = ephem.Moon()
    moon.compute(home)
    sun = ephem.Sun()
    sun.compute(home)

    moonrise = home.next_rising(moon)
    home.date = moonrise
    moon.compute(home)
    sunset = ephem.localtime(home.next_setting(sun, moonrise))
    entry = {
        'date': ephem.localtime(start_date),
        'moon_rise': ephem.localtime(moonrise),
        'moon_ra': moon.ra,
        'moon_dec': moon.dec,
        'moon_alt': moon.alt,
        'moon_az': moon.az,
        'moon_phase': moon.phase,
        'sunset': sunset,
        'sunset_diff': sunset - ephem.localtime(moonrise)
    }
    batch.append(entry)
    #print(entry)
    start_date = ephem.Date(start_date + interval)

#print(batch)

df = pd.DataFrame(batch)
print(df)
df.plot(x='date', y='moon_phase')
plt.show()
