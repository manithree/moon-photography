#!/usr/bin/python3

import wx
from wx.adv import DatePickerCtrl
import sys

# app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
# frame = wx.Frame(None, wx.ID_ANY, "Hello World") # A Frame is a top-level window.
# frame.Show(True)     # Show the frame.
# from wx.adv import DatePickerCtrl
# dpc = DatePickerCtrl(frame)
# button = wx.Button(frame, label='OK')
# dpc.Show(True)
# app.MainLoop()

class Dialog(wx.Dialog):
    def __init__(self, *args, **kwargs):
        super(Dialog, self).__init__(*args, **kwargs)
        self.datepicker = DatePickerCtrl(self)
        self.button = wx.Button(self, label='OK')
        self.button.SetDefault()
        self.button.Bind(wx.EVT_BUTTON, self.onButton)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.datepicker)
        sizer.Add(self.button)
        self.SetSizerAndFit(sizer)

    def onButton(self, event):
        d = self.datepicker.GetValue()
        print(str.format('{year:04d}/{month:02d}/{day:02d}',
                         year=d.year, month=d.month, day=d.day))
        self.Close()
        sys.exit()


class Frame(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(Frame, self).__init__(*args, **kwargs)
        self.button = wx.Button(self, label='Press me to open dialog')
        #self.button.Bind(wx.EVT_BUTTON, self.onButton)
        dialog = Dialog(self, size=(200,30))
        dialog.Show()

app = wx.App()
frame = Frame(None, size=(200,50))
# frame.Show()
app.MainLoop()
