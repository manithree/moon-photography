#!/usr/bin/python3

import ephem
import sys,os
import datetime
import configparser

config = configparser.ConfigParser()
config.read("{HOME}/.config/mooncal.ini".format(HOME=os.environ['HOME']))

home = ephem.Observer()
home.lat = config['location']['lat']
home.lon = config['location']['lon']
home_tz = config['location']['timezone']

if (len(sys.argv) > 1):
    home_date = ephem.date(sys.argv[1])
else:
    home_date = ephem.now()

right_now = ephem.now()

home.date = right_now

moon = ephem.Moon()
moon.compute(home)
sun = ephem.Sun()
sun.compute(home)

print('Moon Right Now {now}'.format(now=ephem.localtime(right_now)))
print('\tα={ra}\tδ={dec}'.format(dec=moon.dec, ra=moon.ra))
print('\taz={az}\talt={alt}'.format(az=moon.az, alt=moon.alt))
home.date = home_date
moon.compute(home)
moonrise = home.next_rising(moon)
moonset = home.next_setting(moon)
sunset = home.next_setting(sun, home_date)
print('\tMoonrise: {next}'.format(
    next=ephem.localtime(moonrise)))
print('\tMoonset: {next}'.format(
    next=ephem.localtime(moonset)))
print()
print('Moon @moonrise:')
home.date = moonrise
moon.compute(home)
print('\tα={ra}\tδ={dec}'.format(dec=moon.dec, ra=moon.ra))
print('\taz={az}\talt={alt}'.format(az=moon.az, alt=moon.alt))
print('\tMoon phase: {phase:2.2f}% illuminated'.format(phase=moon.phase))
print('\tSunset: {sunset}'.format(
    sunset=ephem.localtime(sunset)))
