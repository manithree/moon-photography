 #!/usr/bin/python3
#
# Copyright 2008 Google Inc. All Rights Reserved.

# originally from https://github.com/googlearchive/js-v2-samples/blob/gh-pages/articles-geotagsimple/exif2kml.py by Mano Marks
# according to the git repo under Apache 2.0 license

# use py3exiv2 (not original pyexiv2)


import sys
import xml.dom.minidom
import pyexiv2


def GetHeaders(the_file):

  data = pyexiv2.ImageMetadata(the_file)
  data.read()
  return data


def DmsToDecimal(degree_num, minute_num, second_num):
  """Converts the Degree/Minute/Second formatted GPS data to decimal degrees.

  Returns:
    A deciminal degree.
  """


  degree = degree_num
  minute = float(minute_num)/60
  second = float(second_num)/3600
  return degree + minute + second


def GetGps(data):
  lat_dms = data['Exif.GPSInfo.GPSLatitude'].value
  lon_dms = data['Exif.GPSInfo.GPSLongitude'].value
  latitude = DmsToDecimal(lat_dms[0], lat_dms[1], lat_dms[2])
  longitude = DmsToDecimal(lon_dms[0], lon_dms[1], lon_dms[2])

  if data['Exif.GPSInfo.GPSLatitudeRef'].value == 'S':  latitude *= -1
  if data['Exif.GPSInfo.GPSLongitudeRef'].value == 'W':  longitude *= -1

  altitude = None

  try:
    altitude = data['Exif.GPSInfo.GPSAltitude'].value
    if data['Exif.GPSInfo.GPSAltitudeRef'].value == 1: altitude *= -1

  except KeyError:
    altitude = 0
  
  return latitude, longitude, altitude


def CreateKmlDoc():
  """Creates a KML document."""

  kml_doc = xml.dom.minidom.Document()
  kml_element = kml_doc.createElementNS('http://www.opengis.net/kml/2.2', 'kml')
  kml_element.setAttribute('xmlns', 'http://www.opengis.net/kml/2.2')
  kml_element = kml_doc.appendChild(kml_element)
  document = kml_doc.createElement('Document')
  kml_element.appendChild(document)
  return kml_doc
  

def CreatePhotoPlacemark(kml_doc, file_name, file_iterator):
  photo_id = 'photo%s' % file_iterator
  data = GetHeaders(file_name)
  coords = GetGps(data)
  placemark = kml_doc.createElement('Placemark')
  name = kml_doc.createElement('name')
  try:
    name_text = data['Xmp.dc.title'].value['x-default']
  except KeyError:
    name_text = file_name

  name.appendChild(kml_doc.createTextNode(name_text))

  placemark.appendChild(name)
  point = kml_doc.createElement('Point')
  coordinates = kml_doc.createElement('coordinates')
  coordinates.appendChild(kml_doc.createTextNode('%s,%s,%s' %(coords[1],
                                                              coords[0],
                                                              coords[2])))
  point.appendChild(coordinates)
  placemark.appendChild(point)
  document = kml_doc.getElementsByTagName('Document')[0]
  document.appendChild(placemark)


def CreateKmlFile(file_names, new_file_name):
  """Creates the KML Document with the Photos, and writes it to a file.

  Args:
    file_names: A list object of all the names of the files.
    new_file_name: A string of the name of the new file to be created.
  """
    
  kml_doc = CreateKmlDoc()
  file_iterator = 0
  for filename in file_names:
    CreatePhotoPlacemark(kml_doc, filename, file_iterator)
    file_iterator += 1

  kml_file = open(new_file_name, 'wb')
  kml_file.write(kml_doc.toprettyxml('  ', newl='\n', encoding='utf-8'))
  
  
def main():
# This function was taken from EXIF.py to directly handle 
# command line arguments.

  args = sys.argv[1:]
  if len(args) < 1:
    print('Please try again with a file name')

  else:
    CreateKmlFile(args, 'test.kml')

   
if __name__ == '__main__':
  main()
