#!/usr/bin/env python3
import datetime
from zoneinfo import ZoneInfo

from astral import LocationInfo
from astral.sun import sun
from astral.moon import moonrise, azimuth, phase
from astral.location import Location
city = LocationInfo("Orem", "Utah", "America/Denver", 40.302266, -111.678064)
local_tz = ZoneInfo("America/Denver")
dt = datetime.datetime.now(tz=local_tz)+datetime.timedelta(days=1)

s = sun(city.observer, date=dt)
print((
    f'Dawn:    {s["dawn"]}\n'
    f'Sunrise: {s["sunrise"]}\n'
    f'Noon:    {s["noon"]}\n'
    f'Sunset:  {s["sunset"]}\n'
    f'Dusk:    {s["dusk"]}\n'
))
orem = Location(city)
rise = moonrise(city.observer, dt, city.tzinfo)
print(f'Moonrise:  {rise}')
moon_az = azimuth(city.observer, at = moonrise(city.observer, dt))
print(f'\tazimuth: {moon_az}°')
print(f'\tphase: {phase(dt)}')
