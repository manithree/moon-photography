# moon-photography

Python code and utilities to help schedule the best days/times for photographing the moon

* dp.py - Simple wx date picker.

* mooncal.py - show the moonrise, sunset, and moon phase (% illuminated) for (currently) a single day

* dpmoon - simple script to put the date picker and mooncal.py together


